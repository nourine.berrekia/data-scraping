import csv
import json
import requests
from bs4 import BeautifulSoup
import re

def request_url(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    elements = soup.find_all('li')
    return elements

def findall_elements(elements):
    with open('file.csv','w') as csv_file:
        fieldnames = ['index', 'en_expression']
        # Create a DictWriter to
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter='\t')
        csv_writer.writeheader()
        i = 1
        for element in elements:
            line = re.sub('\s\s+see\s+',' SEE ', element.text).strip()
            print('{}   {}'.format(i, line))
            i += 1
            file = csv_writer.writerow({'index':i,'en_expression':line})
        return file

print(findall_elements(elements=request_url('https://medlineplus.gov/bloodheartandcirculation.html')))












